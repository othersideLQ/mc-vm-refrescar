FROM openjdk:8u131-jre-alpine
MAINTAINER Sergio Robles "practicanteti1@ebizlatin.com"
ENV REFRESHED_AT 2017-07-01 23:20
ARG PORT_EXPOSE=8080
ADD ./app.jar app.jar
RUN sh -c 'touch /app.jar'
EXPOSE ${PORT_EXPOSE}
ENV JAVA_OPTS=""
ENTRYPOINT ["sh", "-c", "java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar"]