package com.ebiz.msrefreshvm;

import java.util.List;

public interface RefreshVMService {
	
	int refreshVM(List<String> for_refresh_list);
	
}
