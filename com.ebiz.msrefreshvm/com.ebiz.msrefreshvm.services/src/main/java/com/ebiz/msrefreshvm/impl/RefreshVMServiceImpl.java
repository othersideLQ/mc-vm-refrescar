package com.ebiz.msrefreshvm.impl;

import com.ebiz.msrefreshvm.RefreshVMRepository;
import com.ebiz.msrefreshvm.RefreshVMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class RefreshVMServiceImpl implements RefreshVMService{
	
	@Autowired
	private RefreshVMRepository refreshVMRepository;
	
	public int refreshVM(List<String> for_refresh_list) {
		
		return refreshVMRepository.refreshVM(for_refresh_list);
	}
	
}
