package com.ebiz.msrefreshvm;

import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
public class MVMap {

	private Map<String, List<String>> for_refresh;

	public Map<String, List<String>> getFor_refresh() {
		return for_refresh;
	}

	public void setFor_refresh(Map<String, List<String>> for_refresh) {
		this.for_refresh = for_refresh;
	}
		
}
