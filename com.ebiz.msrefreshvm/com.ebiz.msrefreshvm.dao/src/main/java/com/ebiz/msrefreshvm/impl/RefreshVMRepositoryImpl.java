package com.ebiz.msrefreshvm.impl;

import com.ebiz.msrefreshvm.RefreshVMRepository;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class RefreshVMRepositoryImpl implements RefreshVMRepository {

	private static final Logger log = LoggerFactory.getLogger(RefreshVMRepositoryImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int refreshVM(List<String> for_refresh) {
		String sql_query = "select refresh_mv('%s');";
		int i = 0;
		for (String vm : for_refresh) {
			try {
				log.info("Refrescando la vm : " + vm);
				jdbcTemplate.execute(String.format(sql_query, vm));
				log.info(vm + " refrescado con exito");
				i++;
			} catch (Exception e) {
				log.error("=== DAO : ocurrio un ERROR al refrescar vm -> {}", vm);
				log.error(e.getMessage(), e);
			}
		}
		return i;
	}

	/*public int refreshVM(List<String> for_refresh) {
		log.info("=== DAO : entro al metodo refreshVM()");
		String sql_query = "select refresh_mv('";
		//int j = 0;
		for (int i=0;i<for_refresh.size();i++) {
			try {
				log.info("Refrescando la vm : " + for_refresh.get(i));
				//jdbcTemplate.update(sql_query, new Object[] {for_refresh.get(i)});
				//j = jdbcTemplate.update(sql_query + for_refresh.get(i) + "');");
				jdbcTemplate.execute(sql_query + for_refresh.get(i) + "');");
				log.info(for_refresh.get(i) + "refrescado con exito");
			}
			catch(Exception e) {
				e.printStackTrace();
				log.info("=== DAO : ocurrio un ERROR");
				log.info(e.getMessage().toString());
				return 0;
			}
		}
		//return j;
		return 1;
		
		log.info("=== DAO : entro al metodo refreshVM()");
		String sql_query = "REFRESH MATERIALIZED VIEW ?";
		try {
			log.info("=== DAO : refrescando vista materializada");
			return jdbcTemplate.update(sql_query, new Object[] {vm_name});
		}
		catch(Exception e) {
			e.printStackTrace();
			log.info("=== DAO : ocurrio un ERROR");
			log.info(e.getMessage().toString());
			return 0;
		}
		
	}*/

}
