package com.ebiz.msrefreshvm;

import java.util.List;

public interface RefreshVMRepository {
	
	int refreshVM(List<String> for_refresh);

}
