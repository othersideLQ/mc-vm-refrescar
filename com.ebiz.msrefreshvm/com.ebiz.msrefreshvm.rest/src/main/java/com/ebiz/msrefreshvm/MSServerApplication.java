package com.ebiz.msrefreshvm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MSServerApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(MSServerApplication.class, args);
	}
	
}
