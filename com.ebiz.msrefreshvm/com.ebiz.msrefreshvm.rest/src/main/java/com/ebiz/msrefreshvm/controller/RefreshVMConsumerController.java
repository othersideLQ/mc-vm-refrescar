package com.ebiz.msrefreshvm.controller;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.ebiz.msrefreshvm.RefreshVMService;
import com.ebiz.events.Evento;
import com.ebiz.events.service.EventManagerService;
import com.ebiz.msrefreshvm.MVMap;

import java.util.List;
import java.util.Map;

@Component
public class RefreshVMConsumerController {

	private static final Logger log = LoggerFactory.getLogger(RefreshVMConsumerController.class);

	@Autowired
	private MVMap mvMap;

	@Autowired
	private EventManagerService eventManagerService;

	@Autowired
	private RefreshVMService refreshVMService;

	@KafkaListener(topicPattern = "${events.topic.input.topicpattern}")
	public void doRefresh(String payload) {
		Evento<JsonNode> evt = eventManagerService.deserializeEvent(payload, JsonNode.class);

		if (evt == null) {
			log.error("El objeto recibido no es un evento");
			return;
		}

		log.info("Evento recibido - event_id: {}", evt.getEvent_id());

		Map<String, List<String>> listMap = mvMap.getFor_refresh();
		String eventName = evt.getEvent_name();

		if (listMap.containsKey(eventName)) {
			int x = refreshVMService.refreshVM(listMap.get(eventName));
			log.info("Se actualizaron {} vistas materializadas para el evento {}", x, eventName);
			//System.out.printf("Vistas materializadas: %s", listMap.get(eventName));
		} else {
			log.info("No hay vistas materializadas para este evento: {}", eventName);
		}
	}

}
